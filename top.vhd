----------------------------------------------------------------------------------
-- Company: Brno University of Technology                                       --
-- Engineer: Alejandro Lopez Palma                                              --
--                                                                              --
-- Design Name:     serial_crc_ccitt_xmodem                                     --
-- Module Name:     top - rtl                                         		    --
-- Project Name:    CRC calculation                           		            --
-- Target Devices:  Coolrunner II                                               --
-- Description:     An example of CRC calculation base on polynomial            --
--                  0x1021/ x^16 + x^12 + x^5 + 1 and init value 0x0000			--
--					(which corresponds with the CCITT-XModem method) using 		--
--					the expansion board as 16-bit input. The design is based    --
--                  on XOR logical doors and serial registers. 					--
--                                                								--
-- Additional:	    The switches and LEDs are been exchanged for a better		--
--					perspective of the board demonstration.	You can see details	--
--					in the 'expansion' file.									--
--																				--
-- CRC calculator:	https://www.lammertbies.nl/comm/info/crc-calculation.html	--
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;


entity top is
    generic(
        NBITS:   integer := 16 -- Number of bits of the binary number
    );
	
	port (
        SW_EXP	:in std_logic_vector(NBITS-1 downto 0); 			-- Input switches (Binary)
		CLK     :in std_logic;                          			-- Clock
		LED_EXP :out std_logic_vector(NBITS-1 downto 0); 			-- Output, LEDs (Binary)
        BTN0    :in std_logic;                         				-- Button 0 working as RST, low active
        D_POS	:out std_logic_vector(3 downto 0); 				    -- Display positions
        D_SEG	:out std_logic_vector(6 downto 0) :=(others => '0') -- Display segments
    );
end entity;

architecture rtl of top is	
  signal lfsr: std_logic_vector (NBITS-1 downto 0);				  -- Set of registers to generate CRC code
  signal crc_out: std_logic_vector (NBITS-1 downto 0);		   	  -- CRC-16 code calculated
  signal clk_10	:std_logic := '0';                                -- Clock 0.1 ms signal 
  signal tmp_10	:std_logic_vector(7 downto 0):=(others => '0');   -- Hexadecimal value
  signal position :std_logic_vector(3 downto 0):=(others => '0'); -- 7-seg displays matrix 
  signal numdis	:std_logic_vector(3 downto 0):=(others => '0');   -- Split BCD value to represent into displays
begin
    -- Logic to CRC Calculation
    process (clk)
		variable i: integer :=NBITS;
	begin
         if (rising_edge(clk)) then
             if (BTN0 = '0') then
                 lfsr <= "0000000000000000";
				 i:=NBITS;
             elsif (i /= 0) then
					 i := i-1;
                     lfsr(0)  <= SW_EXP(i) xor lfsr(15);
                     lfsr(1)  <= lfsr(0);
                     lfsr(2)  <= lfsr(1);
                     lfsr(3)  <= lfsr(2);
                     lfsr(4)  <= lfsr(3);
                     lfsr(5)  <= lfsr(4) xor SW_EXP(i) xor lfsr(15);
                     lfsr(6)  <= lfsr(5);
                     lfsr(7)  <= lfsr(6);
                     lfsr(8)  <= lfsr(7);
                     lfsr(9)  <= lfsr(8);
                     lfsr(10) <= lfsr(9);
                     lfsr(11) <= lfsr(10);
                     lfsr(12) <= lfsr(11) xor SW_EXP(i) xor lfsr(15);
                     lfsr(13) <= lfsr(12);
                     lfsr(14) <= lfsr(13);
					 lfsr(15) <= lfsr(14);
             end if;
         end if;
     end process;
 
     crc_out <= lfsr;

	--Show LEDs according to binary input
	LED_EXP <= SW_EXP;

	
    ----------------------------------------------------------
	-- Show CRC value into the display HEX-base             --
	-- Clock divider to 0.1 ms to make the multiplex    	--
    -- function between displays, giving the impression 	--
    -- every display works at the same time  				--
	----------------------------------------------------------
    process (CLK)
    begin
        if rising_edge(CLK) then
            tmp_10 <= tmp_10 + 1;
            if tmp_10 = x"32" then
                tmp_10 <= x"00";
                clk_10 <= not clk_10;
            end if;
        end if;
    end process;
	----------------------------------------------------------
    

	-- Switch between displays and its corresponding values --
    process (clk_10)
    begin
        if rising_edge(clk_10) then
            if position = "0111" then
                numdis <= crc_out(3 downto 0);
                position <= "1110";
            elsif position = "1110" then
                numdis <= crc_out(7 downto 4);  
                position <= "1101";
            elsif position = "1101" then 
                numdis <= crc_out(11 downto 8); 
                position <= "1011";
            elsif position <= "1011" then
                numdis <= crc_out(15 downto 12);   
                position <= "0111";
            end if;
         end if;
    end process;
    ----------------------------------------------------------
       
    -- Counter to seven-segment display, low active --
    with numdis select                     --          0
        D_SEG <= "1111001" when "0001",     -- 1       ---
                 "0100100" when "0010",     -- 2    5 |   | 1
                 "0110000" when "0011",     -- 3       ---   <- 6
                 "0011001" when "0100",     -- 4    4 |   | 2
                 "0010010" when "0101",     -- 5       ---
                 "0000010" when "0110",     -- 6        3
                 "1111000" when "0111",     -- 7
                 "0000000" when "1000",     -- 8    
                 "0010000" when "1001",     -- 9
				 "0001000" when "1010",		-- A
				 "0000011" when "1011",		-- b
				 "1000110" when "1100",		-- C
				 "0100001" when "1101",		-- d
				 "0000110" when "1110",		-- E
				 "0001110" when "1111",		-- F
                 "1000000" when others;     -- 0
        D_POS <= position;
    ---------------------------------------------------  
end rtl;

